============
Contributors
============

* Francesc Campoy (`@gocampoy`_)
* Raphael Pierzina (`@hackebrot`_)

.. _`@gocampoy`: https://gitlab.com/gocampoy
.. _`@hackebrot`: https://gitlab.com/hackebrot
